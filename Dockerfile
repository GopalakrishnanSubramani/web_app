FROM python:3.9-buster
RUN mkdir -p /app
COPY . /app
WORKDIR /app

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y
RUN pip install -r requirements.txt

CMD ["python3","app.py", "--host", "172.17.0.2", "--port", "5000"]
